
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'pm-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {
  private isLoggedIn: boolean;

  private isAdmin: boolean;
  private loginSubscription: Subscription;
  private componentDestroyed = new Subject<void>();

  constructor(private authService: AuthenticationService,
    private router: Router) { }

  ngOnInit(): void {

    this.authService.currentUser
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (userLogged) => {
          if (userLogged) {
            this.isLoggedIn = true;
            if (userLogged.isAdmin) {
              this.isAdmin = true;
            }
          }
        },
        err => console.error(err)
      );
  }
  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
    this.loginSubscription.unsubscribe();
  }
  private logOut(): void {
    this.authService.logout();
    this.router.navigate(['/welcome']);
    this.isLoggedIn = false;
    this.isAdmin = false;
  }
}


