import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StarComponent } from './star.component';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { ModalComponent } from './modal.component';

@NgModule({
  declarations: [
    StarComponent,
    ModalComponent,
  ],
  exports: [
    StarComponent,
    ModalComponent,
    CommonModule,
    FormsModule,
    MaterialModule,
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ]
})
export class SharedModule { }
