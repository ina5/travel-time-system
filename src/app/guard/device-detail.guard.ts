import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DeviceDetailGuard implements CanActivate {
  constructor(private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean {
    const id: number = +next.url[1].path;
    if (isNaN(id) || id < 1) {
      alert('Invalid device Id');
      this.router.navigate(['/devices']);

      return false;
    }

    return true;
  }
}
