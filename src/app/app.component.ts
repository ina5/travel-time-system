import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'pm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class TravelTimeAppComponent implements OnInit {
  @Output() public sidenavToggle = new EventEmitter();
  constructor() { }

  ngOnInit() {

  }
  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }
}
