import { AdminGuard } from './guard/admin.guard';
import { AuthGuard } from './guard/auth.guard';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { AppRoutingModule } from './routing/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TravelTimeAppComponent } from './app.component';
import { HttpErrorInterceptor } from './helpers/error.interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { NavbarComponent } from './navbar/navbar.component';
import { MaterialModule } from './shared/material.module';
import { WelcomeComponent } from './components/home/welcome.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { LoginModule } from './components/login/login.module';
import { RegisterModule } from './components/register/register.module';
import { ToastrModule } from 'ngx-toastr';


@NgModule({
  declarations: [
    TravelTimeAppComponent,
    WelcomeComponent,
    NavbarComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpClientModule,
    LoginModule,
    RegisterModule,
    AppRoutingModule,
    ToastrModule.forRoot(),

  ],
  bootstrap: [TravelTimeAppComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    AuthGuard,
    AdminGuard
  ]
})
export class AppModule { }
