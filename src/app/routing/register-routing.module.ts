import { RegisterComponent } from './../components/register/register.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';



@NgModule({
    imports: [
        RouterModule.forChild([
            { path: 'register', component: RegisterComponent }
        ])
    ],
    exports: [RouterModule]
})
export class RegisterRoutingModule { }
