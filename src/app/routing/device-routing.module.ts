import { AdminGuard } from './../guard/admin.guard';
import { DeviceDetailComponent } from './../components/devices/device-detail/device-detail.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../guard/auth.guard';
import { CreateDeviceComponent } from '../components/devices/create-device/create-device.component';
import { DeviceListComponent } from '../components/devices/device-list/device-list.component';


const routes: Routes = [
    { path: '', component: DeviceListComponent, canActivate: [AuthGuard] },
    { path: 'add', component: CreateDeviceComponent, canActivate: [AdminGuard] },
    { path: ':id', component: DeviceDetailComponent, canActivate: [AdminGuard] }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DeviceRoutingModule { }
