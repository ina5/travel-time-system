import { LoginComponent } from './../components/login/login.component';

import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';



@NgModule({
    imports: [
        RouterModule.forChild([
            { path: 'login', component: LoginComponent }
        ])
    ],
    exports: [RouterModule]
})
export class LoginRoutingModule { }
