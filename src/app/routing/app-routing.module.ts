
import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { WelcomeComponent } from '../components/home/welcome.component';
import { NotFoundComponent } from '../components/not-found/not-found.component';

const routes: Routes = [
    { path: 'welcome', component: WelcomeComponent },
    { path: '', redirectTo: 'welcome', pathMatch: 'full' },
    { path: 'devices', loadChildren: './../components/devices/device.module#DeviceModule' },
    { path: 'users', loadChildren: './../components/user/user.module#UserModule' },
    { path: 'tablereports', loadChildren: './../components/reports/report.module#ReportModule' },
    { path: 'chartreports', loadChildren: './../components/reports/chart.module#ChartModule' },
    { path: 'not-found', component: NotFoundComponent },
    { path: '**', redirectTo: '/not-found', pathMatch: 'full' }

];

@NgModule({
    imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
    exports: [RouterModule]

})
export class AppRoutingModule { }
