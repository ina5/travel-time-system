import { AddUserComponent } from './../components/user/add-user/add-user.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from '../guard/auth.guard';
import { ListUsersComponent } from '../components/user/list-users/list-users.component';
import { AdminGuard } from '../guard/admin.guard';


const routes: Routes = [
    { path: '', component: ListUsersComponent, canActivate: [AdminGuard] },
    { path: 'add', component: AddUserComponent, canActivate: [AdminGuard] }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule { }
