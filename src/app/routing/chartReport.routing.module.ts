import { AddChartComponent } from './../components/reports/chart-report/add-chart/add-chart.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from '../guard/auth.guard';
import { ListChartComponent } from '../components/reports/chart-report/list-chart/list-chart.component';

const routes: Routes = [
    { path: '', component: ListChartComponent, canActivate: [AuthGuard] },
    { path: 'add', component: AddChartComponent, canActivate: [AuthGuard] },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class ChartReportRoutingModule { }
