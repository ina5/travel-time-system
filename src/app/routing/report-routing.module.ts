
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from '../guard/auth.guard';
import { ListTableComponent } from '../components/reports/table-report/list-table/list-table.component';
import { AddTableComponent } from '../components/reports/table-report/add-table/add-table.component';

const routes: Routes = [
    { path: '', component: ListTableComponent, canActivate: [AuthGuard] },
    { path: 'add', component: AddTableComponent, canActivate: [AuthGuard] },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class ReportRoutingModule { }
