
export class LoggedInUser {

    email: string;
    isAdmin: boolean;
    token?: string;
}
