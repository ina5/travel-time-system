import { Device } from 'src/app/dto-models/device.dto';
export class ChartReportRaw {
    name: string;
    origin: Device;
    destination: Device;
    dates: number[];
    period: number;
}
