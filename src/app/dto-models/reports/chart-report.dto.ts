import { User } from './../user';
import { Device } from './../device.dto';
export class ChartReport {
  name: string;
  origin: Device;
  destination: Device;
  periodInMilliseconds: number;
  startDates: number [];
}
