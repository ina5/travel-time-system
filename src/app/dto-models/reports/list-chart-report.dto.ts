import { Device } from './../device.dto';
export class ListChartReport {
  id: string;
  name: string;
  devices: Device;
  startDates: [];
  periodInMilliseconds: string;
}
