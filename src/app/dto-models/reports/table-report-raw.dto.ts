import { Device } from 'src/app/dto-models/device.dto';
export class TableReportRaw {
    name: string;
    devices: Device[];
    period: string;
    offset?: number;
    typeOffset: string;
}
