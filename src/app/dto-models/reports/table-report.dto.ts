import { Device } from '../device.dto';
import { Offset } from '../offset.dto';

export class TableReport {
    name: string;
    devices: Device[];
    period: string;
    offset?: Offset;
}
