import { Device } from '../device.dto';
import { User } from '../user';

export class TableReportDTO {
    id: string;
    name: string;
    devices: Device[];
    startDateInMilliseconds: number;
    endDateInMilliseconds: number;
    users: User;
}
