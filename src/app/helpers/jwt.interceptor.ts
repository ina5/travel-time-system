import { AuthenticationService } from './../services/authentication.service';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoggedInUser } from '../dto-models/logged-in-user.dto';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(private authenticationService: AuthenticationService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        this.authenticationService.currentUser.subscribe(
            user => {
                if (user && user.token) {
                    request = request.clone({
                        setHeaders: {
                            Authorization: `Bearer ${user.token}`
                        }
                    });
                }
            }
        );


        return next.handle(request);
    }
}
