import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../dto-models/user';
import { tap } from 'rxjs/operators';
import { ListUser } from '../dto-models/list-user.dto';
import { Observable } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class UserService {
    private userUrl: string = 'http://localhost:3000/users';
    private registerUrl: string = 'http://localhost:3000/register';
    constructor(private http: HttpClient) { }

    getAll(): Observable<ListUser[]> {
        return this.http.get<ListUser[]>(this.userUrl)
            .pipe(
                tap(),
            );
    }

    register(user: User): any {
        return this.http.post(this.registerUrl, user);
    }

    delete(ids: string): Observable<any> {
        const url = `${this.userUrl}/${ids}`;
        return this.http.delete(url)
            .pipe();
    }
    addUser(user: User): Observable<any> {
        return this.http.post(this.userUrl, user);
    }
}
