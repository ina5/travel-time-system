import { Device } from '../dto-models/device.dto';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { tap } from 'rxjs/operators';
@Injectable({
    providedIn: 'root'
})
export class DeviceService {
    private deviceUrl: string = 'http://localhost:3000/devices';
    constructor(private http: HttpClient) { }
    private handleError(err: HttpErrorResponse) {
        // don't use in real App
        let errorMessage = '';
        if (err.error instanceof ErrorEvent) {
            errorMessage = `An error occured: ${err.error.message}`;
        } else {
            errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
        }
        console.error(errorMessage);

        return throwError(errorMessage);
    }

    getAll(): Observable<Device[]> {
        return this.http.get<Device[]>(this.deviceUrl)
            .pipe(
                tap(),
            );
    }

    getDevice(id: string): Observable<Device> {
        return this.http.get<Device>(`${this.deviceUrl}/${id}`);
    }

    addDevice(device: Device): Observable<any> {
        device.latitude += '';
        device.longitude += '';
        return this.http.post(this.deviceUrl, device);
    }

    delete(ids: string): Observable<any> {
        const url = `${this.deviceUrl}/${ids}`;
        return this.http.delete(url)
            .pipe();
    }

    update(device: Device): Observable<any> {
        return this.http.put(`${this.deviceUrl}/${device.id}`, device);
    }
}
