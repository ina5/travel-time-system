import { User } from './../dto-models/user';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { LoggedInUser } from '../dto-models/logged-in-user.dto';
import * as jwt_decode from 'jwt-decode';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private url: string = 'http://localhost:3000/login';
    private loggedIn: LoggedInUser;
    private currentUserSubject: BehaviorSubject<LoggedInUser> = new BehaviorSubject<LoggedInUser>(this.getLoggedUser());
    public currentUser = this.currentUserSubject.asObservable();
    constructor(private http: HttpClient) { }


    public get getcurrentUser(): LoggedInUser {
        return this.currentUserSubject.value;
    }

   public login(email: string, password: string): Observable<User> {
        return this.http.post<any>(this.url, { email, password })
            .pipe(tap(token => {
                if (token) {
                    localStorage.setItem('currentUser', token);
                    this.loggedIn = new LoggedInUser();
                    const decodedUser = this.getDecodedAccessToken(localStorage.getItem('currentUser'));
                    this.loggedIn = decodedUser;
                    this.loggedIn.token = token;
                    this.currentUserSubject.next(this.loggedIn);
                }
            }));
    }
   public logout(): void {

        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);

    }
    private getDecodedAccessToken(token: string) {
        try {
            return jwt_decode(token);
        } catch (Error) {
            return Error.message('Invalid token!');
        }
    }

    private getLoggedUser(): LoggedInUser {
        const token = localStorage.getItem('currentUser');
        return token ? this.getDecodedAccessToken(token) : null;
    }
}
