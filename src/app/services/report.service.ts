import { ChartReportRaw } from './../dto-models/reports/chart-report-raw.dto';
import { TableReportDTO } from '../dto-models/reports/table-report-data.dto';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { TableReportRaw } from '../dto-models/reports/table-report-raw.dto';
import { Offset } from '../dto-models/offset.dto';
import { TableReport } from '../dto-models/reports/table-report.dto';
import { Observable } from 'rxjs';
import { ListChartReport } from '../dto-models/reports/list-chart-report.dto';
import { ChartReport } from '../dto-models/reports/chart-report.dto';


@Injectable({
    providedIn: 'root'
})
export class ReportService {
    private apiUrl: string = 'http://ec2-35-158-53-19.eu-central-1.compute.amazonaws.com:8080/api/travelTimeTableData';
    private tableReportUrl: string = 'http://localhost:3000/table-reports';
    private apiChartUrl: string = 'http://ec2-35-158-53-19.eu-central-1.compute.amazonaws.com:8080/api/comparePeriods';
    private chartReportUrl: string = 'http://localhost:3000/chart-reports';
    constructor(private http: HttpClient) { }

    getAllTableReports(): Observable<TableReportDTO[]> {
        return this.http.get<TableReportDTO[]>(this.tableReportUrl)
            .pipe(
                tap(),
            );
    }
    createTableReport(tableReportRaw: TableReportRaw): Observable<any> {
        // check offset radio button value
        const offset = new Offset();

        if (tableReportRaw.typeOffset === '1') {
            offset.hours = tableReportRaw.offset;
            offset.days = 0;
        } else if (tableReportRaw.typeOffset === '2') {
            offset.days = tableReportRaw.offset;
            offset.hours = 0;
        }

        const tableReport = new TableReport();
        tableReport.name = tableReportRaw.name;
        tableReport.devices = tableReportRaw.devices;
        tableReport.offset = offset;
        tableReport.period = tableReportRaw.period;

        return this.http.post(this.tableReportUrl, tableReport);
    }

    getAllChartReports(): Observable<ListChartReport[]> {
        return this.http.get<ListChartReport[]>(this.chartReportUrl)
            .pipe(
                tap(),
            );
    }
    createChartReport(chartReport: ChartReportRaw): Observable<any> {
        const datesInMilliseconds: number[] = [];
        // check for minutes
        if (chartReport.period.toString()[0] === '0') {
            chartReport.period = chartReport.period * 100 * 60000;
        } else {
            chartReport.period = (chartReport.period * 60) * 60000;
        }

        // convert dates to milliseconds
        chartReport.dates.forEach(date => {
            datesInMilliseconds.push(new Date(date).getTime());
        });

        chartReport.dates = datesInMilliseconds;
        const createdChart = new ChartReport();
        createdChart.name = chartReport.name;
        createdChart.origin = chartReport.origin;
        createdChart.destination = chartReport.destination;
        createdChart.startDates = chartReport.dates;
        createdChart.periodInMilliseconds = chartReport.period;

        return this.http.post(this.chartReportUrl, createdChart);

    }
    getTableReportValues(report: TableReportDTO): Observable<TableReportDTO> {

        const devicesIds: string[] = report.devices.map(device => device.id);
        const stringIds: string = devicesIds.join(',');
        const startMilliseconds = report.startDateInMilliseconds;
        const endMilliseconds = report.endDateInMilliseconds;
        const date = `{"from":${startMilliseconds},"to":${endMilliseconds}}`;
        const params = new HttpParams().set('devices', stringIds).set('date', date);

        return this.http.get<TableReportDTO>(this.apiUrl, { params }).pipe();
    }

    getChartReportValues(report: ListChartReport): Observable<ListChartReport> {
        const startDates = report.startDates.map((date: any) => {

            return date.dateInMilliseconds;
        });
        const origin = report.devices[0];
        const destination = report.devices[1];
        const params = new HttpParams()
            .set('originDeviceId', origin.id)
            .set('destinationDeviceId', destination.id)
            .set('startDates', startDates.join(','))
            .set('periodLength', report.periodInMilliseconds);

        return this.http.get<ListChartReport>(this.apiChartUrl, { params }).pipe();
    }

}
