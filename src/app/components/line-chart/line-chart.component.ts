import { Component, NgZone, AfterViewInit, OnDestroy, Input } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { template } from '@angular/core/src/render3';
import { ReportModule } from '../reports/report.module';

am4core.useTheme(am4themes_animated);

@Component({
  selector: 'pm-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements AfterViewInit, OnDestroy {

  private chart: am4charts.XYChart;


constructor(private zone: NgZone) {}

@Input() _temp: any;
@Input() report: any;

ngAfterViewInit() {
  this.zone.runOutsideAngular(() => {
    const chart = am4core.create(`${this.report.id}`, am4charts.XYChart);

  const datesObj = {};
  const colors = [ '#f46842', '#e59165', '#e276b3', '#d775e2', '#75dae2', '#75e2ad', '#a9e275' ];

chart.data = [];
Object.keys(this._temp).forEach((date, index) => {

  this._temp[date].forEach((element, objIndex) => {
    const newObj = {};
    if (!index) {
      datesObj[objIndex] = element['x'];
      newObj[`x${index}`] = element['x'];
    } else {
      newObj[`x${index}`] = datesObj[objIndex];
    }
    newObj[`y${index}`] = element['y'];
    chart.data.push(newObj);
  });

const dateAxis = chart.xAxes.push(new am4charts.DateAxis());
dateAxis.renderer.grid.template.location = 0;
dateAxis.renderer.labels.template.fill = am4core.color('#c4564a');

const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.tooltip.disabled = true;
valueAxis.renderer.labels.template.fill = am4core.color('#c4564a');

valueAxis.renderer.minWidth = 60;

const series = chart.series.push(new am4charts.LineSeries());
series.name = `${date}`;
series.dataFields.dateX = `x${index}`;
series.dataFields.valueY = `y${index}`;
series.tooltipText = '{valueY.value}';
series.fill = am4core.color(colors[index]);
series.stroke = am4core.color(colors[index]);

chart.cursor = new am4charts.XYCursor();

const scrollbarX = new am4charts.XYChartScrollbar();
scrollbarX.series.push(series);
chart.scrollbarX = scrollbarX;

chart.legend = new am4charts.Legend();
chart.legend.parent = chart.plotContainer;
chart.legend.zIndex = 100;

dateAxis.renderer.grid.template.strokeOpacity = 0.07;
valueAxis.renderer.grid.template.strokeOpacity = 0.07;
});

    this.chart = chart;
  });
}

ngOnDestroy() {
  this.zone.runOutsideAngular(() => {
    if (this.chart) {
      this.chart.dispose();
    }
  });
}

}
