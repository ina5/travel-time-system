import { Component, Input, OnInit } from '@angular/core';
import { Icon, Marker, icon, latLng, Map, marker, point, polyline, tileLayer } from 'leaflet';
import { Device } from 'src/app/dto-models/device.dto';
import * as L from 'leaflet';

@Component({
  selector: 'pm-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  private map: L.Map;
  private marker;
  private _selectedDevices: Device[];
  private route = polyline([[24.913923, 67.315833], [24.913922, 67.315837]]);
  private start = marker([24.913923, 67.315833], {
    icon: icon({
      iconSize: [25, 41],
      iconAnchor: [13, 41],
      iconUrl: 'leaflet/marker-icon.png',
      shadowUrl: 'leaflet/marker-shadow.png'
    })
  });

  private defaultIcon: Icon = icon({
    iconUrl: '../../../assets/images/marker-icon.png',
    shadowUrl: '../../../assets/images/marker-shadow.png'
  });

  private end = marker([24.913922, 67.315837], {
    icon: icon({
      iconSize: [25, 41],
      iconAnchor: [13, 41],
      iconUrl: 'leaflet/marker-icon.png',
      shadowUrl: 'leaflet/marker-shadow.png'
    })
  });
  private firstpolyline;
  private coordinates = [];
  private pointList = [];
  constructor() { }

  @Input() set selectedDevices(value: Device[]) {

    if (value.length) {

      this._selectedDevices = value;
      this.route = polyline(this.transformDevices());
      this.marker = marker;

      this.start = marker([this.devices[0].latitude, this.devices[0].longitude], {
        icon: icon({
          iconSize: [25, 41],
          iconAnchor: [13, 41],
          iconUrl: 'leaflet/marker-icon.png',
          shadowUrl: 'leaflet/marker-shadow.png'
        })
      });

      this.end = marker([this.devices[this.devices.length - 1].latitude, this.devices[this.devices.length - 1].longitude], {
        icon: icon({
          iconSize: [25, 41],
          iconAnchor: [13, 41],
          iconUrl: 'leaflet/marker-icon.png',
          shadowUrl: 'leaflet/marker-shadow.png'
        })
      });

      this.map.fitBounds(this.route.getBounds(), {
        padding: point(24, 24),
        maxZoom: 12,
        animate: true
      });

      this.devices.forEach(device => this.coordinates.push([Number(device.latitude), Number(device.longitude)]));

      L.marker(this.coordinates.forEach(coordinate => L.marker(coordinate).addTo(this.map)));

      this.devices.forEach(device => this.pointList.push(new L.LatLng(Number(device.latitude), Number(device.longitude))));

      this.firstpolyline = new L.Polyline(this.pointList, {
        color: 'red',
        weight: 3,
        opacity: 0.5,
        smoothFactor: 1
      });
      this.firstpolyline.addTo(this.map);
    }
  }

  get devices(): Device[] {
    return this._selectedDevices;
  }

  streetMaps = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    detectRetina: true,
  });
  wMaps = tileLayer('http://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
    detectRetina: true,
  });

  layersControl = {
    baseLayers: {
      'Street Maps': this.streetMaps,
      'Wikimedia Maps': this.wMaps
    },
    overlays: {
      'start': this.start,
      'end': this.end,
      'Route': this.route
    }
  };

  // Set the initial set of displayed layers (we could also use the leafletLayers input binding for this)
  options = {
    layers: [this.streetMaps, this.route, this.start, this.end],
    zoom: 7,
    center: latLng([25.342394, 68.171898])
  };
  onMapReady(map: Map) {
    this.map = map;
    map.fitBounds(this.route.getBounds(), {
      padding: point(24, 24),
      maxZoom: 12,
      animate: true
    });
  }
  ngOnInit() {
    Marker.prototype.options.icon = this.defaultIcon;
  }
  private transformDevices(): number[] {

    const newCollection = this.devices.reduce((acc, arrDevices) => {
      const coordinates: number[] = [Number(arrDevices.latitude), Number(arrDevices.longitude)];
      acc.push(coordinates);

      return acc;
    }, []);

    return newCollection;
  }
}
