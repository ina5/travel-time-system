
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './register.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RegisterRoutingModule } from 'src/app/routing/register-routing.module';

@NgModule({
    declarations: [
        RegisterComponent
    ],
    imports: [
        SharedModule,
        RegisterRoutingModule,
        ReactiveFormsModule,
    ]
})
export class RegisterModule { }
