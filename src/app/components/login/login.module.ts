import { LoginComponent } from './login.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { LoginRoutingModule } from 'src/app/routing/login-routing.module';

@NgModule({
    declarations: [
        LoginComponent
    ],
    imports: [
        SharedModule,
        LoginRoutingModule,
        ReactiveFormsModule,
    ]
})
export class LoginModule { }
