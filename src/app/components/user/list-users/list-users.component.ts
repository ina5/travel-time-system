import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { MatTableDataSource, MatPaginator, MatSort, MatCheckboxModule } from '@angular/material';
import { ListUser } from 'src/app/dto-models/list-user.dto';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'pm-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  private pageTitle: string = 'User List';
  private errorMessage: string;
  private email: string;
  private position: number;
  private users: ListUser[] = [];
  private displayedColumns: string[] = ['email', 'select'];
  private dataSource;

  private initialSelection = [];
  private allowMultiSelect: boolean = true;
  private selection: SelectionModel<ListUser> = new SelectionModel<ListUser>();
  private loading: boolean = false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private userService: UserService) { }


  ngOnInit(): void {

    this.userService.getAll().subscribe(
      users => {
        this.users = users;
        this.dataSource = new MatTableDataSource<ListUser>(this.users);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.selection = new SelectionModel<ListUser>(this.allowMultiSelect, this.initialSelection);
      },
      error => this.errorMessage = <any>error
    );
  }
  private isAllSelected() {

    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;

    return numSelected === numRows;
  }
  private masterToggle() {

    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  private delete() {
    const getIds = this.selection.selected.map(user => user.id);

    this.loading = true;

    getIds.forEach((id: string) => {
      this.userService.delete(id).subscribe();

      // delete from device collection
      this.users.splice(this.users
        .indexOf(this.users
          .find(user => user.id === id)), 1);
    });
    this.dataSource = new MatTableDataSource<ListUser>(this.users);

    this.loading = false;
  }
}
