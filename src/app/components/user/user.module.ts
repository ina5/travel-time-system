import { AddUserComponent } from './add-user/add-user.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ListUsersComponent } from './list-users/list-users.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { UserRoutingModule } from 'src/app/routing/user-routing.module';

@NgModule({
    declarations: [
        AddUserComponent,
        ListUsersComponent
    ],
    imports: [
        SharedModule,
        UserRoutingModule,
        ReactiveFormsModule,
    ]
})
export class UserModule { }
