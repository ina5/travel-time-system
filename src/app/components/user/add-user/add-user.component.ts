
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'pm-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  addUserForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private alertService: AlertService
  ) { }

  ngOnInit() {

    this.alertService.subject.subscribe();
    this.addUserForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

  }
  get controls() { return this.addUserForm.controls; }

  onSubmit() {

    this.submitted = true;
    this.alertService.error('Invalid form!');
    if (this.addUserForm.invalid) {

      return this.alertService.error('Invalid form!');
    }

    this.loading = true;

    this.userService.addUser(this.addUserForm.value)
      .pipe()
      .subscribe(
        data => {

          this.alertService.success('Added successful', true);
          this.router.navigate(['/users']);
        },
        error => {

          this.alertService.error(error);
          this.loading = false;
        });
  }
}
