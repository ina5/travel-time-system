import { ListChartComponent } from './chart-report/list-chart/list-chart.component';
import { AddChartComponent } from './chart-report/add-chart/add-chart.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ChartReportRoutingModule } from 'src/app/routing/chartReport.routing.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { LineChartComponent } from '../line-chart/line-chart.component';

@NgModule({
    declarations: [
        AddChartComponent,
        ListChartComponent,
        LineChartComponent
    ],
    imports: [
        SharedModule,
        ChartReportRoutingModule,
        ReactiveFormsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
    ]
})
export class ChartModule { }
