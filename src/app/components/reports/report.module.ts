import { MapComponent } from './../map/map.component';
import { ListChartComponent } from './chart-report/list-chart/list-chart.component';
import { ListTableComponent } from './table-report/list-table/list-table.component';
import { AddChartComponent } from './chart-report/add-chart/add-chart.component';
import { AddTableComponent } from './table-report/add-table/add-table.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReportRoutingModule } from 'src/app/routing/report-routing.module';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
@NgModule({
    declarations: [
        AddTableComponent,
        ListTableComponent,
        MapComponent
    ],
    imports: [
        SharedModule,
        ReportRoutingModule,
        ReactiveFormsModule,
        NgxMaterialTimepickerModule.forRoot(),
        LeafletModule.forRoot(),
    ]
})
export class ReportModule { }
