import { Component, OnInit } from '@angular/core';
import { ReportService } from 'src/app/services/report.service';
import { ListChartReport } from 'src/app/dto-models/reports/list-chart-report.dto';

@Component({
  selector: 'pm-list-chart',
  templateUrl: './list-chart.component.html',
  styleUrls: ['./list-chart.component.css']
})
export class ListChartComponent implements OnInit {

  private chartReports: ListChartReport[] = [];
  private allInfo: Map<string, any> = new Map();
  private data = [];
  private dates = [];
  private temp = [];
  private errorMessage: string;
  private isExpanded: boolean;

  constructor(private reportService: ReportService) { }

  ngOnInit() {

    this.reportService.getAllChartReports().subscribe(
      chartReports => {
        chartReports.forEach(async chartReport => {
          await this.callReportService(chartReport);
          this.chartReports = chartReports;
        }
        );
      },
      error => {this.errorMessage = <any>error; }
    );
  }

  private expand(report: ListChartReport) {
    this.isExpanded = !this.isExpanded;
    this.temp = [];
    this.temp = this.allInfo.get(report.id);

    this.data.push((<any>Object).values(this.temp));
    this.dates.push((<any>Object).keys(this.temp));
  }

  private async callReportService(report: ListChartReport) {
    await this.reportService.getChartReportValues(report)
      .subscribe(
        reportResult => {
          this.allInfo.set(report.id, reportResult);
        }
      );
  }

}
