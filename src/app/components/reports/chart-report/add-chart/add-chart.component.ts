import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { DeviceService } from 'src/app/services/device.service';
import { ReportService } from 'src/app/services/report.service';
import { Device } from 'src/app/dto-models/device.dto';

@Component({
  selector: 'pm-add-chart',
  templateUrl: './add-chart.component.html',
  styleUrls: ['./add-chart.component.css']
})
export class AddChartComponent implements OnInit {
  private devicesObjectsMap: Map<string, Device> = new Map();
  private devicesObjects: Device[] = [];
  private createChartReportForm: FormGroup;
  private errorMessage: string;
  private loading = false;
  private submitted = false;
  private returnUrl: string;
  private dates: string[] = [];
  private maxDate = new Date();
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private deviceService: DeviceService,
    private reportService: ReportService

  ) { }

  ngOnInit(): void {
    this.deviceService.getAll().subscribe(
      devices => {
        devices.forEach(device => {
          this.devicesObjectsMap.set(device.name, device);
        });
        this.devicesObjects = devices;
      },
      error => this.errorMessage = <any>error
    );
    this.createChartReportForm = this.formBuilder.group({
      name: ['', Validators.required],
      origin: ['', Validators.required],
      destination: ['', Validators.required],
      dates: [''],
      period: ['', Validators.required],

    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  get controls() {
    return this.createChartReportForm.controls;
  }

  set date(value: string) {
    if (value) {
      this.dates.push(value);
    }

  }

  onSubmit() {
    if (this.dates.length === 1) {

      // throw Error;
    }

    this.submitted = true;

    if (this.createChartReportForm.invalid) {

      return this.alertService.error('Invalid form!');
    }
    this.loading = true;

    this.createChartReportForm.value.dates = this.dates;

    this.createChartReportForm.value.origin = this.devicesObjects
      .find(device => device.name === this.createChartReportForm.value.origin);
    this.createChartReportForm.value.destination = this.devicesObjects
      .find(device => device.name === this.createChartReportForm.value.destination);

    this.reportService.createChartReport(this.createChartReportForm.value)
      .pipe()
      .subscribe(
        data => {
          this.alertService.success('Added successful', true);
          this.router.navigate(['/chartreports']);
        },
        error => {

          this.alertService.error(error);
          this.loading = false;
        });
  }
  private selectDate() { }
}
