import { Router, ActivatedRoute } from '@angular/router';
import { Device } from 'src/app/dto-models/device.dto';
import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { DeviceService } from 'src/app/services/device.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { ReportService } from 'src/app/services/report.service';

@Component({
  selector: 'pm-add-table',
  templateUrl: './add-table.component.html',
  styleUrls: ['./add-table.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddTableComponent implements OnInit {
  private devicesObjects: Device[] = [];
  private devicesObjectsMap: Map<string, Device> = new Map();
  private current_selected: string[] = [];
  private errorMessage: string;
  private createTableReportForm: FormGroup;
  private loading = false;
  private submitted = false;
  private returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private deviceService: DeviceService,
    private reportService: ReportService

  ) { }
  ngOnInit(): void {

    this.deviceService.getAll().subscribe(
      devices => {
        devices.forEach(device => {
          this.devicesObjectsMap.set(device.name, device);
        });
        this.devicesObjects = devices;
      },
      error => this.errorMessage = <any>error
    );

    this.createTableReportForm = this.formBuilder.group({
      name: ['', Validators.required],
      devices: [''],
      period: ['', Validators.required],
      offset: ['', Validators.min(0) && Validators.max(100)],
      typeOffset: ['', Validators.required]
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

  }
  get controls() {
    return this.createTableReportForm.controls;
  }

  onSubmit() {
    // find devices object by name
    this.devicesObjects = [];
    this.current_selected.forEach(nameDevice => {
      const value = this.devicesObjectsMap.get(nameDevice);
      this.devicesObjects.push(value);
    });

    this.createTableReportForm.value.devices = this.devicesObjects;

    this.submitted = true;

    if (this.createTableReportForm.invalid) {

      return this.alertService.error('Invalid form!');
    }
    this.loading = true;
    // transform date period
    this.reportService.createTableReport(this.createTableReportForm.value)
      .pipe()
      .subscribe(
        data => {
          this.alertService.success('Added successful', true);
          this.router.navigate(['/tablereports']);
        },
        error => {

          this.alertService.error(error);
          this.loading = false;
        });
  }

  onSelection(e, v) {
    if (this.current_selected.includes(e.option.value)) {
      const index = this.current_selected.indexOf(e.option.value);
      if (index > -1) {
        this.current_selected.splice(index, 1);
      }
    } else {
      this.current_selected.push(e.option.value);
    }

  }
}
