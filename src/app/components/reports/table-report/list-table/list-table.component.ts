
import { Component, OnInit } from '@angular/core';
import { ReportService } from 'src/app/services/report.service';
import { TableReportDTO } from 'src/app/dto-models/reports/table-report-data.dto';
import { Device } from 'src/app/dto-models/device.dto';

@Component({
  selector: 'pm-list-table',
  templateUrl: './list-table.component.html',
  styleUrls: ['./list-table.component.css']
})
export class ListTableComponent implements OnInit {
  private data: Map<string, any> = new Map();
  private devicesIds: string[];

  private devicesNames: string[];
  private reportResults = [];
  private dev_stripped = {};

  private tableReports: TableReportDTO[] = [];
  private errorMessage: string;
  private selectedDevices: Device[] = [];

  constructor(private reportService: ReportService) { }
  ngOnInit(): void {

    this.reportService.getAllTableReports().subscribe(
      tableReports => {
        tableReports.forEach(async tableReport => await this.callReportService(tableReport));
        this.tableReports = tableReports;
      },
      error => this.errorMessage = <any>error
    );
  }
  private expand(report: TableReportDTO) {
    this.selectedDevices = [];
    this.reportResults = [];
    // take Id's and Name's
    this.devicesIds = report.devices.map(device => device.id);
    this.devicesNames = report.devices.map(device => device.name);

    // result data match devicesId to devicesName
    report.devices.forEach(dev => {
      this.dev_stripped[dev.id] = dev.name;
    });

    // take result for the table
    const temp = this.data.get(report.id);
    this.devicesIds.forEach(id => (this.reportResults.push(temp[id])));
    this.selectedDevices = report.devices;
  }
  private async callReportService(report: TableReportDTO) {

    // get result from Api for all reports
    await this.reportService.getTableReportValues(report).subscribe(
      reportResult => {
        this.data.set(report.id, reportResult);
      }
    );
  }
  private convertToDate(time: string): string {

    return new Date(Number(time)).toLocaleString();
  }
}

