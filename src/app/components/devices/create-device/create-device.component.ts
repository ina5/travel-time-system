
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { DeviceService } from 'src/app/services/device.service';

@Component({
  selector: 'pm-create-device',
  templateUrl: './create-device.component.html',
  styleUrls: ['./create-device.component.css']
})
export class CreateDeviceComponent implements OnInit {
  private addDeviceForm: FormGroup;
  private loading = false;
  private submitted = false;
  private returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private deviceService: DeviceService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.addDeviceForm = this.formBuilder.group({
      name: ['', Validators.required],
      longitude: ['', Validators.required],
      latitude: ['', Validators.required],
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  get controls() { return this.addDeviceForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.addDeviceForm.invalid) {

      return this.alertService.error('Invalid form!');
    }
    this.loading = true;

    this.deviceService.addDevice(this.addDeviceForm.value)
      .subscribe(
        data => {
          this.alertService.success('Added successful', true);
          this.router.navigate(['/devices']);
        },
        error => {

          this.alertService.error(error);
          this.loading = false;
          return error;
        }
      );
  }
}
