import { AuthenticationService } from 'src/app/services/authentication.service';

import { Component, OnInit, ViewChild } from '@angular/core';
import { DeviceService } from 'src/app/services/device.service';
import { Device } from 'src/app/dto-models/device.dto';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { of } from 'rxjs';

@Component({
    selector: 'pm-device-list',
    templateUrl: './device-list.component.html',
    styleUrls: ['./device-list.components.css']
})
export class DeviceListComponent implements OnInit {

    private pageTitle: string = 'Devices List';
    private errorMessage: string;
    private _listFilter: string;
    private position: number;
    private displayedColumns: string[] = ['name', 'longitude', 'latitude'];
    private dataSource;

    private initialSelection = [];
    private allowMultiSelect = true;
    private selection: SelectionModel<Device> = new SelectionModel<Device>();
    private filteredDevices: Device[];
    private devices: Device[] = [];
    private loading: boolean = false;
    private isInAdminRole: boolean = false;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(
        private deviceService: DeviceService,
        private authService: AuthenticationService) {
    }
    get listFilter(): string {
        return this._listFilter;
    }
    set listFilter(value: string) {
        this._listFilter = value;
        this.filteredDevices = this.listFilter ? this.performFilter(this.listFilter) : this.devices;
    }

    onRatingClicked(message: string): void {
        this.pageTitle = 'Devices list: ' + message;
    }
    performFilter(filterBy: string): Device[] {
        filterBy = filterBy.toLocaleLowerCase();

        return this.devices.filter((device: Device) => device.name.toLocaleLowerCase().indexOf(filterBy) !== -1);
    }
    ngOnInit(): void {

        this.authService.currentUser
            .pipe()
            .subscribe(
                (userLogged) => {
                    if (userLogged !== null) {
                        if (userLogged.isAdmin) {
                            this.isInAdminRole = true;
                            this.displayedColumns = ['name', 'longitude', 'latitude', 'select'];
                        }
                    }
                },
                err => console.error(err)
            );

        this.deviceService.getAll().subscribe(
            devices => {
                this.devices = devices;
                this.dataSource = new MatTableDataSource<Device>(this.devices);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                this.selection = new SelectionModel<Device>(this.allowMultiSelect, this.initialSelection);
                this.filteredDevices = this.devices;
            },
            error => this.errorMessage = <any>error
        );
    }

    private isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }
    private masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }

    private delete() {
        const getIds = this.selection.selected.map(device => device.id);

        this.loading = true;

        getIds.forEach((id: string) => {
            this.deviceService.delete(id).subscribe();

            // delete from device collection
            this.devices.splice(this.devices
                .indexOf(this.devices
                    .find(device => device.id === id)), 1);
        });
        this.dataSource = new MatTableDataSource<Device>(this.devices);

        this.loading = false;
    }
}
