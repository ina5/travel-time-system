import { DeviceDetailComponent } from './device-detail/device-detail.component';

import { NgModule } from '@angular/core';
import { DeviceListComponent } from './device-list/device-list.component';

import { CreateDeviceComponent } from './create-device/create-device.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ConvertToSpacesPipe } from 'src/app/shared/convert-to-spaces.pipe';
import { SharedModule } from 'src/app/shared/shared.module';
import { DeviceRoutingModule } from 'src/app/routing/device-routing.module';

@NgModule({
  declarations: [
    DeviceListComponent,
    DeviceDetailComponent,
    ConvertToSpacesPipe,
    CreateDeviceComponent
  ],
  imports: [
    SharedModule,
    DeviceRoutingModule,
    ReactiveFormsModule
  ],
  exports: [DeviceListComponent]
})
export class DeviceModule { }
