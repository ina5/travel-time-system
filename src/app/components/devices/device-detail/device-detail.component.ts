import { DeviceService } from './../../../services/device.service';
import { Device } from 'src/app/dto-models/device.dto';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
@Component({
  templateUrl: './device-detail.component.html',
  styleUrls: ['./device-detail.component.css']
})
export class DeviceDetailComponent implements OnInit {

  device: Device;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly deviceService: DeviceService) { }

    getDevice(): void {
      const id = this.route.snapshot.paramMap.get('id');

      this.deviceService.getDevice(id).subscribe(device => this.device = device);
    }

  ngOnInit() {
    this.getDevice();
  }
  onBack(): void {
    this.router.navigate(['/devices']);
  }

  save(): void {
    this.deviceService.update(this.device)
      .subscribe(() => this.onBack());
  }
}
