# **Travel Time System Project**


## What our project do

Our system is only for admin clients. When you register you automaticaly are admin and can: 
- Create users;
- Delete multiple users;
- Create Device for crossroad;
- Delete multiple devices;
- Edit device;

We have functionality for creating table report. 
- You need to write table report name.
- You need to choose minimum 2 devices.
- You need to select period.
Then you can see table report list and expand the current table report to check the result between devices.

We have functionality for creating chart report.
- You need to write chart report name.
- You need to choose only 2 devices - start and end point.
- You need to select minimum two dates.


> Our URL Routing:

| Routes                            |    Admin       |    ViewUser       |    Public   |     
| :---                              |     :---:      |         :---: |         ---: |
| /welcome                          |   ✔️           |    ✔️          |     ✔️        |
| /register                         |   ✔️           |    ✔️          |     ✔️        |
| /login                            |   ✔️           |    ✔️          |     ✔️        |
| /devices                          |   ✔️           |    ✘          |     ✘        |
| /devices/add                      |   ✔️           |    ✘          |     ✘        |
| /devices/id                       |   ✔️           |    ✘          |     ✘        |
| /users                            |   ✔️           |    ✘          |     ✘        |
| /users/add                        |   ✔️           |    ✘          |     ✘        |
| /tablereports/add                 |   ✔️           |    ✔️          |     ✘        |
| /tablereports                     |   ✔️           |    ✔️          |     ✘        |
| /chartreports/add                 |   ✔️           |    ✔️          |     ✘        |
| /chartreports                     |   ✔️           |    ✔️          |     ✘        |


## Getting Started

If you want to test and run the project on your local machine you need to clone repository into a brand new folder. Follow the steps:

1. Follow the link: [TTS-FrontEnd](https://gitlab.com/ina5/travel-time-system) and clone the frontEnd project;
2. Open travel-time-system project and run `npm install` command;
3. Follow the link: [TTS-BackEnd](https://gitlab.com/ina5/travel-time-system-backend) and clone the backEnd project;
4. Open travel-time-system-backend project and run `npm install` command;
5. To start the backEnd project run `npm run start:dev` command;
6. To start the frontEnd project and open it directly in browser run `npm start` command;

For more information you can open package.json file in the project and check different commands in scripts property - "scripts". 

